 class base
{
	int a;
	private int b;
	base(int _a,int _b)
	{
		a=_a;
		b=_b;
	}
}
	
 class derived extends base
{
	private int x;
	int y;
	derived(int a,int b, int c, int d){
		super(c,d );
		x=a;
		y=b;
	}
	
}
public class tryObjectComp
{
	public static void main(String[] args) {

		String a ="abc";
		String b = "abc";

		base b1 = new base(10,20);
		
		
		derived d1 = new derived(11,12,13,14);

		derived d2 = new derived(11,12,13,14);

		derived d3 = new derived(1,2,3,4);

		derived d4 = new derived( 5,6,7,8);

		derived d5 = null;

		try
		{
			compareObj.compObj(b1,d1);
			compareObj.compObj(d2,d1);
			compareObj.compObj(d3,d4);
			compareObj.compObj(a,b);
			compareObj.compObj(d5,d4);


		}
		catch( Exception e)
		{
			System.out.println("Exception occured  ");
		}
		/*catch( Exception e)
		{
			System.out.println("Exception occured  "+e.getMessage()+e.getCause());
		}*/
	}
}
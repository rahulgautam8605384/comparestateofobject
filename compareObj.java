//This program defined method compObj to compare state of two objects
//without the use of comparator
//@author- Rahul Gautam

import java.lang.reflect.Field;
import java.lang.Class;
 class compareObj
{

	static boolean traverse(Object obj1,Object obj2) throws Exception //This method is used to comapre individual fields of the object
	{
		boolean result=false;
		Field[] elements1 = obj1.getClass().getDeclaredFields();
			for(Field i : elements1)
			{
				i.setAccessible(true);								//to access the private	fields of a class				
				if(i.get(obj1).equals(i.get(obj2)))
				{
					result = true;
				}
				else
				{
					result = false;
					break;
				}
			}
			return result;											//return true if the objects are equal

	}

	public static void compObj(Object obj1, Object obj2) throws IllegalAccessException
	{
		

		boolean result = false;

		if((obj1 instanceof obj2.getClass()) || (obj2 instanceof obj1.getClass()){
			System.out.println("Provided object belongs to same hierarchy");
		}
		else if(obj1.getClass()!=obj2.getClass())   						//This is to check wheather the supplied object is from same class
		{
			result = false;
		}
		else if( obj1.getClass().equals(obj2.getClass()))   		//If the objects belongs to the same class their fieds will be compared in following block
		{
			result = compareObj.traverse(obj1, obj2);
		}

		if(result)
		{
			Class c = obj1.getClass();
			c= c.getSuperclass();
			while(c !=null)
			{
				if(compareObj.traverse(c,obj2))
				{
					result=true;
				}else
				{
					result= false;
					break;
				}
				c= c.getSuperclass();								//this call to getSuperclass() makes sure the fields of all the superclass are compared
			}
		}

		if(result == true)
		{
			System.out.println("The Objects are equal");
		}else 
		{
			System.out.println("The Objects are unequal");
		}

		//return result;

	}
}